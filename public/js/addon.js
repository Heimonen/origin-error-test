/* add-on script */

$(document).ready(function () {

  // The following functions use the HipChat Javascript API
  // https://developer.atlassian.com/hipchat/guide/javascript-api

  //To send a message to the HipChat room, you need to send a request to the add-on back-end
  function sayHello() {
    return new Promise(function(resolve, reject) {
      //Ask HipChat for a JWT token
      HipChat.auth.withToken(function (err, token) {
        if (!err) {
          //Then, make an AJAX call to the add-on backend, including the JWT token
          //Server-side, the JWT token is validated using the middleware function addon.authenticate()
          $.ajax(
              {
                type: 'POST',
                url: '/send_notification',
                headers: {'Authorization': 'JWT ' + token},
                dataType: 'json',
                data: {messageTitle: 'Hello World!'},
                success: function() {
                  resolve();
                },
                error: function() {
                  reject();
                }
              });
        } else {
          reject();
        }
      });
    });
  }

  /* Functions used by sidebar.hbs */

  $('#open_dialog').on('click', function () {
    openDialog();
  });  

  function openDialog() {
    HipChat.dialog.open({
      key: 'sample.dialog',
    });
  }

  /* Functions used by dialog.hbs */

  //Register a listener for the dialog button - primary action "say Hello"
  HipChat.register({
    'receive-parameters': receiveParameters,
    'dialog-button-click': function (event, closeDialog) {
      if (event.action === "sample.dialog.action") {
        //If the user clicked on the primary dialog action declared in the atlassian-connect.json descriptor:
        sayHello().then(function(response) {
          HipChat.sidebar.openView({
            key: 'sample.sidebar'
          });
          closeDialog(true);
        }).catch(function(err) {
          console.error('could not send message!');
          closeDialog(false);
        });
      }
    }
  });

  function receiveParameters(params) {
    console.log('Parameters received!');
  }

});